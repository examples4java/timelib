/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timeutils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 *
 * @author tez
 */
public final class MeasureTime {
    
    enum TimeOptions {
        SECONDS(0),
        MICROS(1),
        MILLIS(2),
        MINS(3),
        CURRTIME(4),
        seconds(0),
        micros(1),
        millis(2),
        mins(3),
        currtime(4);
        
        private final int val;
        
        private TimeOptions(int val) {this.val = val;}
        
        public int get() {return val;}
    }
    
    final class TimeStruct {
        long start,stop,_suspend;
        boolean running;
        
        void timeStart() {_suspend=0;start=System.nanoTime();running=true;}
        long suspend() {
            stop=System.nanoTime();
            running=false;
            return stop-start-_suspend;
        }
        void activate() {
            _suspend+=System.nanoTime()-stop;
            running=true;
        }
        boolean isRunning() {return running;}
        long getTime() {return -_suspend + ((running) ? System.nanoTime()-start : stop-start);}
        Object[] getFullTime() {return new Object[] {getTime(),running};}
        long timeStop() {stop=System.nanoTime();running=false;return stop-start-_suspend;}
        long getSuspend() {return _suspend;}
        boolean contains(long val) {return val==start||val==stop||val==getTime();}
    }

    Map<String, TimeStruct> timeHistory;

    public MeasureTime() {
        this(false, "default");
    }
    
    public MeasureTime(boolean state) {
        this(state,"default");
    }
    
    public MeasureTime(String name) {
        this(false, name);
    }
    
    public MeasureTime(boolean state, String name) {
        timeHistory = new HashMap<>();
        //by nie powtarzać kodu wykonane zostają dwie linie
        //jedna dodaje i startuje liczenie
        //druga, jeżeli liczenie ma się nie rozpocząć kończy je
        //efektywność dyskusyjna, redukcja kodu (i rozwijalność) zachowana
        startMeasure(name);
        if (!state) stopMeasure(name);
    }
    
    public void startMeasure() {
        startMeasure("default");
    }
    
    public void startMeasure(String name) {
        if (timeHistory.keySet().contains(name))
            timeHistory.get(name).timeStart();
        else {
            TimeStruct ts = new TimeStruct();
            ts.start = ts.stop = 0;
            ts.running = true;
            timeHistory.put(name, ts);
            //rekurencyjnie wywołujemy start obliczen - brak powtórzeń operacji
            startMeasure(name);            
        }             
    }
    
    public boolean stopMeasure() {
        return stopMeasure("default");
    }
    
    public boolean stopMeasure(String name) {
        if (timeHistory.keySet().contains(name)) {
            timeHistory.get(name).timeStop();
            return true;
        }
        return false;
    }
    
    public boolean suspendMeasure() {
        return suspendMeasure("default");
    }
    
    public boolean suspendMeasure(String name) {
        if (timeHistory.keySet().contains(name)) {
            timeHistory.get(name).suspend();
            return true;
        }
        return false;
    }
    
    public void activateMeasure() {
        activateMeasure("default");
    }
    
    public void activateMeasure(String name) {
        if (timeHistory.keySet().contains(name))
            timeHistory.get(name).activate();
    }
    
    public boolean getRunningStatus() {
        return getRunningStatus("default");
    }
    
    public boolean getRunningStatus(String name) {
        return  (timeHistory.keySet().contains(name)) ?
            timeHistory.get(name).isRunning() : false;
    }
    
    public final long getMeasure() {
        return getMeasure(0, "default");
    }
    
    public final long getMeasure(String s) {
        //return getMeasuer(s, "default");
        try {
            return getMeasure(TimeOptions.valueOf(s).get());
        }
        catch (Exception e) {
            System.err.println("Brak podanej opcji, wypisuję w domyślnym przedziale (sekundy!)");
        }
        if (timeHistory.keySet().contains(s)) return getMeasure(0,s);
        return getMeasure();
    }
    
    public final long getMeasure(String s, String name) {
         try {
            return getMeasure(TimeOptions.valueOf(s).get(),name);
        }
        catch (Exception e) {
            System.err.println("Brak podanej opcji, wypisuję w domyślnym przedziale (sekundy!)");
        }
        if (timeHistory.keySet().contains(name)) return getMeasure(0,name);
        return getMeasure();
    }
    
    public final static long getTimeStamp() {
        return System.currentTimeMillis();
    }
    
    public final long getMeasure(int op) {
        return getMeasure(op, "default");
    }
    
    public final long getMeasure(int op, String name) {
        long l = timeHistory.get(name).getTime();
        switch(op) {
            case 0: return TimeUnit.NANOSECONDS.toSeconds(l);
            case 1: return TimeUnit.NANOSECONDS.toMicros(l);
            case 2: return TimeUnit.NANOSECONDS.toMillis(l);
            case 3: return TimeUnit.NANOSECONDS.toMinutes(l);
            case 4: return TimeUnit.NANOSECONDS.toMillis(System.nanoTime());
        } 
        return l;
    }
    
    
    
    public final String getFormattedMeasure(int op, String name) {
        Object o[] = timeHistory.get(name).getFullTime();
        long l = Long.parseLong(String.valueOf(o[0]));
        String ret = "Aktualny mierzony czas to ";
        switch(op) {
            case 0: ret += TimeUnit.NANOSECONDS.toSeconds(l) + " sekund"; break;
            case 1: ret += TimeUnit.NANOSECONDS.toMicros(l) + " mikrosekund"; break;
            case 2: ret += TimeUnit.NANOSECONDS.toMillis(l) + " milisekund"; break;
            case 3: ret += TimeUnit.NANOSECONDS.toMinutes(l) + " minut"; break;
            case 4: ret += TimeUnit.NANOSECONDS.toMillis(System.nanoTime()) + " nanosekund";
        } 
        ret += ", aktualnie stoper " + (Boolean.parseBoolean(String.valueOf(o[1])) ? "pracuje" : "nie pracuje.");
        return ret;
    }
    
    public long getSavedTime(String name) {
        return getMeasure(0, name);
    }
    
    public long getSavedTime(String s, String name) {
        return getMeasure(s,name);
    }
    
    public Map<String,TimeStruct> searchSavedTime(ArrayList<String> s) {
        Map<String,TimeStruct> r = new HashMap<>();
        s.forEach(n-> r.put(n, timeHistory.get(n)));
        return r;
    }
    
    public List<String> getTimesName() {
        return new ArrayList<>(timeHistory.keySet());//Arrays.asList((String[])timeHistory.keySet().toArray());
    }
    
    public List<String> searchSavedTime(List<TimeStruct> l) {
        List<String> r = new ArrayList<>();
        timeHistory.forEach((k,v)-> {for(TimeStruct ts:l) if (v.equals(ts))r.add(k);});
        return r;
    }
    
    public List<TimeStruct> searchSavedTime(long val) {
        return timeHistory.values().stream().
                filter(time->time.contains(val)).
                collect(Collectors.toList());
    }
    
    public Map<String,TimeStruct> getSavedTime() {
        return (timeHistory != null) ? timeHistory : new HashMap<>();
    }
    
    public void clearSavedTimes() {
        if (timeHistory == null) timeHistory = new HashMap<>();
        timeHistory.clear();
    }   
    
}
